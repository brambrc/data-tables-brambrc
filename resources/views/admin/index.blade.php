@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            <div class="card-body">
              <div class="table-responsive">
                <div class="panel panel-default">
                {{(!! $dataTable->table() !!)}}
              </div>
            </div>
            </div>
          </div>
        </div>
</div>
@stop
@push('scripts')
  {!! $dataTable->scripts() !!}
@endpush
