@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
  <form method="POST" action="{{route('updateSuper', ['id' => $get->id])}}">
  {{csrf_field()}}
  <div class="row">
      <div class="col-md-6">
          <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label">{{ __('Employee Number') }}</label>
              <div class="col-md-12">
                  <input id="employee_number" type="text" class="form-control" name="employee_no" value="{{ $get->employee_number }}" required autofocus />
              </div>
          </div>
          <div class="form-group row">
              <label for="birthplace" class="col-md-4 col-form-label">{{ __('Birthplace') }}</label>
              <div class="col-md-12">
                  <input id="birthplace" type="text" class="form-control" name="birthplace" value="{{ $get->birthplace }}" required/>
              </div>
          </div>
          <div class="form-group row">
              <label for="age" class="col-md-4 col-form-label">{{ __('Age') }}</label>
              <div class="col-md-12">
                  <input id="age" type="number" class="form-control" name="age" value="{{ $get->age }}" min="1" max="99" required/>
              </div>
          </div>
          <div class="form-group row">
              <label for="status" class="col-md-4 col-form-label">{{ __('Status') }}</label>
              <div class="col-md-12">
                  <select name="status" class="form-control" required/>
                  <option @if ($get->status == 'active') selected @endif value="active">Active</option>
                  <option @if ($get->status == 'deadactive') selected @endif value="deadactive">Deadactive</option>
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <label for="startdate" class="col-md-4 col-form-label">{{ __('Start Date') }}</label>
              <div class="col-md-12">
                  <input id="startdate" type="date" class="form-control" name="startdate" value="{{ old('startdates') }}" required/>
              </div>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>
              <div class="col-md-12">
                  <input id="name" type="name" class="form-control" name="name" value="{{ old('name') }}" required/>
              </div>
          </div>
          <div class="form-group row">
              <label for="birthdate" class="col-md-4 col-form-label">{{ __('Birth Date') }}</label>
              <div class="col-md-12">
                  <input id="birthdate" type="date" class="form-control" name="birthdate" value="{{ old('birthdate') }}" required/>
              </div>
          </div>
          <div class="form-group row">
              <label for="occupation" class="col-md-4 col-form-label">{{ __('Occupation') }}</label>
              <div class="col-md-12">
                  <input id="occupation" type="text" class="form-control" name="occupation" value="{{ old('occupation') }}" required/>
              </div>
          </div>
          <div class="form-group row">
              <label for="gender" class="col-md-4 col-form-label">{{ __('Gender') }}</label>
              <div class="col-md-12">
                  <select name="gender" class="form-control" required/>
                  <option @if ($get->gender == 'male') selected @endif value="male">Male</option>
                  <option @if ($get->gender == 'female') selected @endif value="female">Female</option>
                  <option @if ($get->gender == 'other') selected @endif value="other">Other</option>
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <label for="address" class="col-md-4 col-form-label">{{ __('Address') }}</label>
              <div class="col-md-12">
                  <textarea id="address" class="form-control" name="address" required/></textarea>
              </div>
          </div>
      </div>
      <div class="form-group row mb-0">
          <div class="col-md-8 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  Update
              </button>
          </div>
  </div>
</form>
                </div>
            </div>
          </div>
  </div>

  @stop
