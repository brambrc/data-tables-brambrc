<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data-info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_no');
            $table->string('name');
            $table->longtext('address');
            $table->string('birthplace');
            $table->date('birthdate');
            $table->string('age');
            $table->string('occupation');
            $table->enum('status', ['active', 'deadactive']);
            $table->enum('gender', ['male', 'female', 'other']);
            $table->date('startdate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data-info');
    }
}
