<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DataInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 50; $i++){

          DB::table('data-info')->insert([
              			'employee_no' => $faker->numberBetween(1000,9000),
                    'name' => $faker->name,
                    'address' => $faker->address,
              			'birthplace' => $faker->cityName,
                    'birthdate' => $faker->date,
                    'age' => $faker->numberBetween(18,99),
                    'occupation' => $faker->jobTitle,
                    'status' => $faker->randomElement(['active', 'deadactive']),
                    'gender' => $faker->randomElement(['male', 'female', 'other']),
                    'startdate' => $faker->date,
              		]);

        }
    }
}
