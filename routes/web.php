<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('superadmin')->middleware('role:superadmin')->group(function(){
Route::get('/dash', 'HomeController@dashSuper')->name('dashSuper');
Route::get('/resource', 'ApiController@index')->name('homeSuperAdmin');
Route::post('/post-data', 'HomeController@store')->name('storeSuper');
Route::get('/edit-data/{id}', 'HomeController@edit')->name('editSuper');
Route::get('/delete-data/{id}', 'HomeController@destroy')->name('destroySuper');
Route::post('/update-data/{id}', 'HomeController@update')->name('updateSuper');
});

Route::prefix('admin')->middleware('role:admin')->group(function(){
Route::get('/dash', 'HomeController@dashAdmin')->name('dashAdmin');
Route::get('/resource/admin', 'ApiController@index')->name('homeAdmin');
});
