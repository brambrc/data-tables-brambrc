<?php

namespace App\DataTables;

use App\DataInfo;
use App\DataTables\InfoDataTable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class InfoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
    }

    public function query(InfoDataTable $model)
    {
        // return $model->newQuery();
        $data = DataInfo::select('data-info.*');
        return $this->applyScopes($data);
    }


    public function html()
    {
      return $this->builder()
                 ->columns($this->getColumns())
                 ->parameters([
                     'dom' => 'Bfrtip',
                     'buttons' => ['csv', 'excel', 'print'],
                 ]);
    }


    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('employee_no'),
            Column::make('name'),
            Column::make('address'),
            Column::make('birthplace'),
            Column::make('birthdate'),
            Column::make('age'),
            Column::make('occupation'),
            Column::make('status'),
            Column::make('gender'),
            Column::make('startdate'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Info_' . date('YmdHis');
    }
}
