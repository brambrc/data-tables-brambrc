<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\DataInfo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $User = User::where('id', Auth::id())->first();
        if($User->role == 'superadmin'){
          return redirect()->route('dashSuper');
        } else {
          return redirect()->route('dashAdmin');
        }
    }

    public function dashSuper()
    {
      return redirect()->route('homeSuperAdmin');
    }

    public function dashAdmin()
    {
      return redirect()->route('homeAdmin');
    }



    public function store(Request $request)
    {
            $store = new DataInfo;
            $store->employee_no = $request->employee_no;
            $store->name = $request->name;
            $store->address = $request->address;
            $store->birthplace = $request->birthplace;
            $store->birthdate = $request->birthdate;
            $store->age = $request->age;
            $store->occupation = $request->occupation;
            $store->status = $request->status;
            $store->gender = $request->gender;
            $store->startdate = $request->startdate;
            $store->save();
            return redirect()->route('dashSuper');
    }

    public function edit($id)
    {
      $data['get'] = DataInfo::findOrFail($id);
      return view('superadmin.edit', $data);
    }

    public function update(Request $request, $id)
    {
          $update = DataInfo::findOrFail($id);
          $store->employee_no = $request->employee_no;
          $store->name = $request->name;
          $store->address = $request->address;
          $store->birthplace = $request->birthplace;
          $store->birthdate = $request->birthdate;
          $store->age = $request->age;
          $store->occupation = $request->occupation;
          $store->status = $request->status;
          $store->gender = $request->gender;
          $store->startdate = $request->startdate;
          $store->save();
          return redirect()->route('dashSuper');
    }

    public function destroy($id)
    {
      $delete = DataInfo::findOrFail($id);
      $delete->delete();
      return redirect()->route('dashSuper');
    }
}
