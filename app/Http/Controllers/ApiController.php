<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\DataTables\InfoDataTable;
use Illuminate\Http\Request;
use App\User;
use App\DataInfo;
use DataTables;

class ApiController extends Controller
{

  // public function __construct()
  //  {
  //
  //  }
  // public function api(Request $request)
  // {
  //   $data = DataInfo::all();
  //   return datatables()->of($data)
  //   ->addIndexColumn()
  //   ->addColumn('action', function($row){
  //   $btn = '<a href="'.$row->id.'" class="edit btn btn-success btn-sm">Edit</a> <a href="'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
  //   return $btn;
  //   })
  //   ->rawColumns(['action'])
  //               ->make(true);
  // }

  public function index(InfoDataTable $dataTable)
  {


    $User = User::where('id', Auth::id())->first();
    if($User->role == 'superadmin'){
      // dd($dataTable);
    return $dataTable->render('superadmin.index');
      } else {
    return $dataTable->render('admin.index');
      }
  }

}
