<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataInfo extends Model
{
  protected $table = 'data-info';
  protected $primaryKey = 'id';
  protected $fillable = [
      'employee_no', 'name', 'address', 'birthplace', 'birthdate', 'age', 'occupation', 'status', 'gender', 'startdate'
  ];
}
